# Tiny Zed

The tiny .zshrc



## What? Why?

This project started because someone benchmarked oh-my-zsh (which is still awesome btw) and saw that it slowed down your shell quite a bit.

I didn't believe it was true at the time but for fun I thought I could try the grml config, the one that made Zsh my favorite shell. And he was right, **oh-my-zsh is slow**. (Note: Responsiveness also factors in subjective feeling. I didn't run a benchmark like the original guy so maybe it's just my brain that tricked me)

So I started switching all Zsh configurations with grml but noticed one thing: It was GPL3. Now I actually use GPL3 a lot myself but I thought it wasn't justified in this case. So I went ahead and learnt some Zsh config-fu.

This result is this repository. Actually it should be named sloth ZSH because I didn't plan on cutting down features. Everything is lazy loaded. There is just no need to call some git function if there is no git for example.

I have to say though grml still beat my config. So why is it in use?

### Our use-case SBCs

SBCs are cool little devices. On paper they absolutly rock (*pun not intended*) but you almost everytime have to get your hands dirty to unlock the full potential. No plug and play. And on my way to get those devices to top speed I didn't want to miss my favorite shell. 

That is also why I choose to disable history saving on disk. 9 out 10 times I will run those scripts on a SD card. And if there is one thing they hate it is logging.

## Setup

Just run `setup.sh` and it will take care of everything. It will only install those scripts for the current user so you have to execute it with each use that wants to use it.

Make sure that you have a version on python on your system. Both 2 and 3 work. It is used to dynamically load and append the lazyload list. If you want to add your own functions just put them into `$HOME/.config/zfuncs`.

### Notes

- It also sources `.profile` and `.zprofile` because every DM kinda tried to ignore them and I think you should have one place at least for your private settings and shell auto execution.

- Try also our other project [baumarkt](https://gitlab.com/SirJson/baumarkt). It extends your shell with more commands that are way more complex then what we ship here. I think in there is something for everyone that's why I called it baumarkt ;-)

### 




