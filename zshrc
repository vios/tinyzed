###
# Tinyzsh
###

HISTFILE=~/.zsh_history
HISTSIZE=1000
SAVEHIST=0
export COMP_CACHE_DIR=${COMP_CACHE_DIR:-${ZDOTDIR:-$HOME}/.cache}
export USR_FUNCS="$HOME/Scripts/zfuncs"
export LSCOLORS="exfxcxdxbxegedabagacad"
export CLICOLOR=true
export DOTNET_CLI_TELEMETRY_OPTOUT=1 # No one want's MS to spy around ;-)
export ZSHCFG="$HOME/.zshrc"
[ -f "$HOME/.zprofile" ] && source $HOME/.zprofile
[ -f "$HOME/.profile" ] && source $HOME/.profile

# Use Comp Cache
if [[ ! -d ${COMP_CACHE_DIR} ]]; then
    command mkdir -p "${COMP_CACHE_DIR}"
fi

if [[ ! -d ${USR_FUNCS} ]]; then
    command mkdir -p "${USR_FUNCS}"
fi


fpath=($USR_FUNCS "${fpath[@]}")

USERFUNCLIST=$(python -c 'import os; print("\n".join(list(map(lambda y: "autoload -Uz {a}".format(a=y),os.listdir(str(os.environ["USR_FUNCS"]))))))')

eval "printf $USERFUNCLIST"
zstyle ':completion:*' use-cache yes
zstyle ':completion:*:complete:*' cache-path "${COMP_CACHE_DIR}"
zstyle ':completion:*' completer _expand _complete _ignored _approximate
zstyle ':completion:*' file-sort name
zstyle ':completion:*' group-name ''
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list 'm:{[:lower:]}={[:upper:]}' 'm:{[:lower:][:upper:]}={[:upper:][:lower:]}' 'r:|[._-]=** r:|=**' 'l:|=* r:|=*'
zstyle ':completion:*' menu select=1
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' special-dirs true
zstyle :compinstall filename '/home/drdoom/.zshrc'

setopt NO_BG_NICE
setopt NO_HUP
setopt NO_LIST_BEEP
setopt LOCAL_OPTIONS
setopt LOCAL_TRAPS
setopt HIST_VERIFY
setopt SHARE_HISTORY
setopt EXTENDED_HISTORY
setopt PROMPT_SUBST
setopt CORRECT
setopt COMPLETE_IN_WORD
setopt IGNORE_EOF
setopt HISTALLOWCLOBBER
setopt NONOMATCH
setopt PRINTEXITVALUE
setopt APPEND_HISTORY
setopt INC_APPEND_HISTORY SHARE_HISTORY
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_REDUCE_BLANKS
setopt HIST_NO_STORE
setopt COMPLETE_ALIASES
autoload -Uz compinit
compinit
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -f ~/lab_comp.zsh ] && source ~/lab_comp.zsh

autoload colors && colors
installed "nvim" && export SU_EDITOR="nvim" || export SU_EDITOR="nano"

git=$(command -v git)
###
# Custom Prompt
###

CURDIR="%B%U%d%u$(prompt_git)$(need_push)%b "

USER_PROMPT="$CURDIR%{%(?.%F{green}.%F{red})%}► %{%f%}"
USER_RPROMPT="%F{cyan}%n@%B%m% %F{yellow} ☻%{%f%}%{%b%}"

ROOT_PROMPT="$CURDIR%{%(?.%F{yellow}.%F{red})%}❯ %{%f%}"
ROOT_RPROMPT="%F{red}%n@%B%m %F{yellow}☢%{%f%}%{%b%}"

[ $EUID -ne 0 ] && export RPROMPT=$USER_RPROMPT || export RPROMPT=$ROOT_RPROMPT
[ $EUID -ne 0 ] && export PROMPT=$USER_PROMPT || export PROMPT=$ROOT_PROMPT

## Switching shell safely and efficiently? http://www.zsh.org/mla/workers/2001/msg02410.html
bash() {
    NO_SWITCH="yes" command bash "$@"
}

###
# Git helper
###




###
# ZLE tweaks
###

## define word separators (for stuff like backward-word, forward-word, backward-kill-word,..)
WORDCHARS='*?_-.[]~=/&;!#$%^(){}<>' # the default

# just type '...' to get '../..'
rationalise-dot() {
    local MATCH
    if [[ $LBUFFER =~ '(^|/| |	|'$'\n''|\||;|&)\.\.$' ]]; then
        LBUFFER+=/
        zle self-insert
        zle self-insert
    else
        zle self-insert
    fi
}

zle -N rationalise-dot
bindkey . rationalise-dot
# without this, typing a . aborts incremental history search
bindkey -M isearch . self-insert
bindkey '\eq' push-line-or-edit

###
# Misc.Tweaks
###

## ctrl-s will no longer freeze the terminal.
stty erase "^?"

bind_lsd() {
    alias l="lsd -1 --group-dirs first"
    alias ll="lsd --long --classify --group-dirs first --timesort --all"
    alias lt="lsd --long --classify --group-dirs first --tree --depth 3"
    alias ls="lsd"
}

aliasing_ls() {
    alias ll='ls -lh'
    alias la='ls -A'
    alias lc='ls -CF'
    alias l='ls -lah'
}

installed "lsd" && bind_lsd || aliasing_ls
installed "fd" && alias find='fd'
installed "bat" && alias cat='bat'
installed "rg" && alias grep='grep'
installed "nvim" && alias vim='nvim'


CLEAN=$(echo $PATH | sd ':' '\n' | sort | uniq | sd '\n' ':')
export PATH=${CLEAN:0:-1}
# The following lines were added by compinstall


# End of lines added by compinstall
